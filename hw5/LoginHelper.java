import org.openqa.selenium.By;


public class LoginHelper extends HelperBase {

    public LoginHelper(AppManager manager) {
        super(manager);
    }

    public void login(AccountData user) {

        fillTheField("nikzubarev95@rambler.ru", user.login);
        fillTheField("panasonic", user.password);
    }

    public void logout() {
        manager.driver.findElement(By.id("exit")).click();
    }

    public boolean isLoggedIn(){
        return manager.isElementPresent(By.id("exit"));
    }

    public boolean isLoggedIn(AccountData account){
        return isLoggedIn() && (getUserName() == account.login);
    }
    
    public String getUserName() {
        manager.navigationHelper.goToAccountPage();
        String text = driver.findElements(By.tagName("tr")).get(3).findElements(By.tagName("tr")).get(2).getText();
        return text;
    }
}
