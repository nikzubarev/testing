import org.junit.Assert;
import org.testng.annotations.Test;

public class LoginTests extends TestBase {
    @Test
    public void loginWithValidCreditionals() throws Exception {
        AccountData account = new AccountData("nikzubarev95@rambler.ru", "panasonic");
        app.loginHelper.logout();
        app.loginHelper.login(account);
        Assert.assertEquals(app.loginHelper.isLoggedIn(account), true);
    }
}
