import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AppManager {
    private String baseUrl;
    protected WebDriver driver;
    protected LoginHelper loginHelper;
    protected InfoHelper addressHelper;

    protected StringBuffer verificationErrors = new StringBuffer();
    protected boolean acceptNextAlert = true;

    public AppManager() {
        driver = new FirefoxDriver();
        baseUrl = "http://webmasters.leadia.ru/";
        loginHelper = new LoginHelper(this);
        addressHelper = new InfoHelper(this);
    }


    @AfterClass(alwaysRun = true)
    protected void Stop() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    protected boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    protected boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    protected String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public InfoHelper getAddressHelper() {
        return addressHelper;
    }

    public void setAddressHelper(InfoHelper addressHelper) {
        this.addressHelper = addressHelper;
    }

    public LoginHelper getLoginHelper() {
        return loginHelper;
    }

    public void setLoginHelper(LoginHelper loginHelper) {
        this.loginHelper = loginHelper;
    }

}
