import org.openqa.selenium.By;

public class InfoHelper extends HelperBase {

    public InfoHelper(AppManager manager) {
        super(manager);
    }

    public void addAddress(InfoData address) {
        fillTheField("address.firstName", address.firstName);
        fillTheField("address.surname", address.surname);
        fillTheSelect("address.cityIso", address.cityIso);
        fillTheField("address.phone", address.phone);
        driver.findElement(By.xpath("css=button.btn.btn-large")).click();
    }
}
