
public class InfoData {
    String firstName;
    String surname;
    String cityIso;
    String postcode;
    String phone;

    public InfoData(String firstName, String surname, String cityIso, String phone) {
        this.firstName = firstName;
        this.surname = surname;
        this.cityIso = cityIso;
        this.phone = phone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }



    public String getCityIso() {
        return cityIso;
    }

    public void setCityIso(String cityIso) {
        this.cityIso = cityIso;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
