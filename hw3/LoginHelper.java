import org.openqa.selenium.By;


public class LoginHelper extends HelperBase {

    public LoginHelper(AppManager manager) {
        super(manager);
    }

    public void login(AccountData user) {

        fillTheField("username", user.login);
        fillTheField("password", user.password);
    }

    public void logout() {
        manager.driver.findElement(By.id("exit")).click();
    }

}
