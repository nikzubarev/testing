using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;

namespace AutoTests {
    [TestFixture]
    public class SimpleOperationTests : TestBase {
        [Test]
        public void AdditionTest() {
            OperationDataInt items = new OperationDataInt() {
                First = 9,
                Sign = "-",
                Second = 3
            };

            manager.Operations.EnterNumber(items.First);
            manager.Operations.EnterSign(items.Sign);
            manager.Operations.EnterNumber(items.Second);
            manager.Operations.Result();

            string result = manager.Operations.GetResult();
            string shouldBe = manager.Operations.Calculate(items);

            NUnit.Framework.Assert.AreEqual(result, shouldBe);
        }
    }
}